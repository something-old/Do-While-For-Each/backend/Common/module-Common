package dwfe.modules.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Validated
@Configuration
@ConfigurationProperties(prefix = "dwfe.common")
public class CommonConfigProperties implements InitializingBean
{
  private final static Logger log = LoggerFactory.getLogger(CommonConfigProperties.class);

  private String api;

  private Resource resource = new Resource();

  private Captcha captcha;

  @Override
  public void afterPropertiesSet() throws Exception
  {
    log.info(toString());
  }

  public static class Resource
  {
    // Third-party
    private String googleCaptchaValidate = "/google-captcha-validate";

    public String getGoogleCaptchaValidate()
    {
      return googleCaptchaValidate;
    }

    public void setGoogleCaptchaValidate(String googleCaptchaValidate)
    {
      this.googleCaptchaValidate = googleCaptchaValidate;
    }
  }

  public static class Captcha
  {
    @NotBlank
    private String googleSecretKey;

    public String getGoogleSecretKey()
    {
      return googleSecretKey;
    }

    public void setGoogleSecretKey(String googleSecretKey)
    {
      this.googleSecretKey = googleSecretKey;
    }
  }


  public String getApi()
  {
    return api;
  }

  public void setApi(String api)
  {
    this.api = api;
  }

  public Resource getResource()
  {
    return resource;
  }

  public void setResource(Resource resource)
  {
    this.resource = resource;
  }

  public Captcha getCaptcha()
  {
    return captcha;
  }

  public void setCaptcha(Captcha captcha)
  {
    this.captcha = captcha;
  }

  @Override
  public String toString()
  {
    return String.format("%n%n" +
                    "-====================================================-%n" +
                    "|                       Common                       |%n" +
                    "|----------------------------------------------------|%n" +
                    "|                                                     %n" +
                    "| API version                       %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| API Resources                                       %n" +
                    "|                                                     %n" +
                    "|   Third-party:                                      %n" +
                    "|      %s%n" +
                    "|                                                     %n" +
                    "|                                                     %n" +
                    "| Is Third-party initialized?                         %n" +
                    "|   Google Captcha                  %s%n" +
                    "|_____________________________________________________%n%n",
            api,

            // Third-party
            resource.googleCaptchaValidate,

            // Is Third-party initialized?
            captcha.googleSecretKey != null
    );
  }
}
